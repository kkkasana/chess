package services;

import org.springframework.stereotype.Service;

@Service
public class RookPostionsService {
	
	static String getrookPositions(int i, int j) {
		String result = "";
		for (int k = 1; k <= 8; k++) {
			if (k != i) {
				String row = ChessDataService.valuedataMap.get(String.valueOf(k));
				result += row + String.valueOf(j) + " ";
			}

		}
		for (int l = 1; l <= 8; l++) {
			if (l != j) {
				String row = ChessDataService.valuedataMap.get(String.valueOf(i));
				result += row + String.valueOf(l) + " ";
			}

		}
		return result;

	}
	

}
