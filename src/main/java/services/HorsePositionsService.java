package services;

import org.springframework.stereotype.Service;

@Service
public class HorsePositionsService {

	static String gethorsePositions(int i, int j) {
		String result = "";
		
		int x1, x2, x3, x4, x5, x6, x7, x8;
		int y1, y2, y3, y4, y5, y6, y7, y8;
		x1 = i + 2;
		y1 = j - 1;
		if (ChessDataService.isSafe(x1, y1)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x1));
			result += row + String.valueOf(y1) + " ";

		}
		x2 = i + 1;
		y2 = j - 2;
		if (ChessDataService.isSafe(x2, y2)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x2));
			result += row + String.valueOf(y2) + " ";

		}
		x3 = i - 1;
		y3 = j - 2;
		if (ChessDataService.isSafe(x3, y3)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x3));
			result += row + String.valueOf(y3) + " ";

		}
		x4 = i - 2;
		y4 = j - 1;
		if (ChessDataService.isSafe(x4, y4)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x4));
			result += row + String.valueOf(y4) + " ";

		}
		x5 = i - 2;
		y5 = j + 1;
		if (ChessDataService.isSafe(x5, y5)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x5));
			result += row + String.valueOf(y5) + " ";

		}
		x6 = i - 1;
		y6 = j + 2;
		if (ChessDataService.isSafe(x6, y6)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x6));
			result += row + String.valueOf(y6) + " ";

		}
		x7 = i + 1;
		y7 = j + 2;
		if (ChessDataService.isSafe(x7, y7)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x7));
			result += row + String.valueOf(y7) + " ";

		}
		x8 = i + 2;
		y8 = j + 1;
		if (ChessDataService.isSafe(x8, y8)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x8));
			result += row + String.valueOf(y8) + " ";

		}
		return result;

	}

}
