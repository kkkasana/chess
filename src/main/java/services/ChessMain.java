package services;

import java.util.Map;
import java.util.Scanner;

public class ChessMain {
	

	public static void main(String[] args) {
		 Map<String, String> keydataMap =ChessDataService.keydataMap;
		 Map<String, String> valuedataMap =ChessDataService.valuedataMap;
		
		keydataMap.put("A", "1");
		keydataMap.put("B", "2");
		keydataMap.put("C", "3");
		keydataMap.put("D", "4");
		keydataMap.put("E", "5");
		keydataMap.put("F", "6");
		keydataMap.put("G", "7");
		keydataMap.put("H", "8");
		
		valuedataMap.put("1", "A");
		valuedataMap.put("2", "B");
		valuedataMap.put("3", "C");
		valuedataMap.put("4", "D");
		valuedataMap.put("5", "E");
		valuedataMap.put("6", "F");
		valuedataMap.put("7", "G");
		valuedataMap.put("8", "H");
		
		Scanner s= new Scanner(System.in);
		String input=s.nextLine();
		String inputArr[]=input.split(" ");
		String chessType=inputArr[0];
		String position=inputArr[1];
		int i=Integer.parseInt(keydataMap.get(position.split("")[0]));
		int j=Integer.parseInt(position.split("")[1]);
		if(chessType.equalsIgnoreCase("Horse")) {
			System.out.println(HorsePositionsService.gethorsePositions(i, j));
		}
		if(chessType.equalsIgnoreCase("King")) {
			System.out.println(KingPositionsService.getkingPositions(i, j));
		}
		
		if(chessType.equalsIgnoreCase("Bishop")) {
			System.out.println(BishopPositionsService.getbishopPositions(i, j));
		}
		if(chessType.equalsIgnoreCase("Rook")) {
			System.out.println(RookPostionsService.getrookPositions(i, j));
		}
		if(chessType.equalsIgnoreCase("Pawn")) {
			System.out.println(PawnPositionsService.getpawnPositions(i, j));
		}
		if(chessType.equalsIgnoreCase("Queen")) {
			System.out.println(BishopPositionsService.getbishopPositions(i, j));
			System.out.println(RookPostionsService.getrookPositions(i, j));
		}
		
		// TODO Auto-generated method stub

	}

}
