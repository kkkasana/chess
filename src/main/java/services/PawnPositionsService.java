package services;

import org.springframework.stereotype.Service;

@Service
public class PawnPositionsService {
	
	static String getpawnPositions(int i, int j) {
		String result = "";
		int x1, x2, x3, x4, x5, x6, x7, x8;
		int y1, y2, y3, y4, y5, y6, y7, y8;
		x1 = i ;
		y1 = j + 1;
		if (ChessDataService.isSafe(x1, y1)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x1));
			result += row + String.valueOf(y1) + " ";

		}
		x2 = i + 1;
		y2 = j + 1;
		if (ChessDataService.isSafe(x2, y2)) {
			String row = ChessDataService.valuedataMap.get(String.valueOf(x2));
			result += row + String.valueOf(y2) + " ";
		}
		return result;

		}

}
