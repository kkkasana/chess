package services;

import org.springframework.stereotype.Service;

@Service
public class BishopPositionsService {
	
	static String getbishopPositions(int i, int j) {
		String result = "";
		int x1=i;
		int y1=j;
		while(x1>0 && y1<8) {
			x1--;
			y1++;
				String row = ChessDataService.valuedataMap.get(String.valueOf(x1));
				result += row + String.valueOf(y1) + " ";
			}
		while(i<8 && j>0) {
			i++;
			j--;
				String row = ChessDataService.valuedataMap.get(String.valueOf(i));
				result += row + String.valueOf(j) + " ";
			}

		
		return result;

	}

}
