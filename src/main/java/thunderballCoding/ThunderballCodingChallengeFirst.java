package thunderballCoding;

import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ThunderballCodingChallengeFirst {

	private final OkHttpClient httpClient = new OkHttpClient();
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	public static void main(String[] args) throws Exception {
		ThunderballCodingChallengeFirst obj = new ThunderballCodingChallengeFirst();
		String s = obj.sendGet();
		int result = 0;
		for (int i = 1; i < s.length() - 1; i++) {
			result *= 26;
			result += s.charAt(i) - 'A' + 1;
		}

		System.out.println(result);
		obj.sendPost(result);

		// TODO Auto-generated method stub

	}

	private String sendGet() throws Exception {

		Request request = new Request.Builder().url("https://thunderball.technogise.com/api/testcase")
				.addHeader("Authorization",
						"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTYyLCJmaXJzdE5hbWUiOiJrcmlzaGFuIiwibGFzdE5hbWUiOiJrYXNhbmEiLCJlbWFpbCI6ImtyaXNoYW5rYXNhbmE4NEBnbWFpbC5jb20iLCJyb2xlIjoiY2FuZGlkYXRlIiwiY3JlYXRlZEF0IjoiMjAyMC0xMC0xM1QwNDoyODo1My4wMTBaIiwidXBkYXRlZEF0IjoiMjAyMC0xMC0xM1QwNDoyODo1My4wMTBaIiwiaWF0IjoxNjAyNzc3MzkzLCJleHAiOjE2MDI4NjM3OTN9.i7GWTgky2TQwRBFItgizYHe8zFsyL7f0n0HCYK9EJwo") // add
																																																																																																						// request
																																																																																																						// headers
				.addHeader("Content-Type", "application/json").build();

		try (Response response = httpClient.newCall(request).execute()) {

			if (!response.isSuccessful())
				throw new IOException("Unexpected code " + response);

			String content = response.body().string();
			JsonObject responseObj1 = (new Gson()).fromJson(content, JsonObject.class).getAsJsonObject();
			String responseMessage = responseObj1.get("column_title").toString();
			return responseMessage;
		}

	}

	private void sendPost(int result) throws Exception {

		RequestBody formBody = new FormBody.Builder().add("answer", "'number' : " + result).build();

		Request request = new Request.Builder().url("https://thunderball.technogise.com/api/testcase")
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization",
						"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTYyLCJmaXJzdE5hbWUiOiJrcmlzaGFuIiwibGFzdE5hbWUiOiJrYXNhbmEiLCJlbWFpbCI6ImtyaXNoYW5rYXNhbmE4NEBnbWFpbC5jb20iLCJyb2xlIjoiY2FuZGlkYXRlIiwiY3JlYXRlZEF0IjoiMjAyMC0xMC0xM1QwNDoyODo1My4wMTBaIiwidXBkYXRlZEF0IjoiMjAyMC0xMC0xM1QwNDoyODo1My4wMTBaIiwiaWF0IjoxNjAyNzc3MzkzLCJleHAiOjE2MDI4NjM3OTN9.i7GWTgky2TQwRBFItgizYHe8zFsyL7f0n0HCYK9EJwo")
				.post(formBody).build();

		try (Response response = httpClient.newCall(request).execute()) {

			if (!response.isSuccessful())
				throw new IOException("Unexpected code " + response);

			// Get response body
			System.out.println(response.body().string());
		}

	}

}
